# The Missing Image

Web Development Trouble Shooting Exercises

This is a series of web development troubleshooting exercises built around a single question. Why isn't the image displaying?

## The Symptom

This will always be the same. The image is missing. It is your job to figure out why.

## The Exercises

The exercises will start simple and gradually increase in complexity as you progress through the exercises. Each exercise will be categorized into environments.

1. HTML
1. CSS
1. Regular JavaScript
1. React
1. Redux
1. Express
1. JavsScript template engines (EJS and Pug)

## Difficulty Progression

Each environment will start with a single problem. Each exercise in the the environment will continue adding one or more problem until you move onto the next environment.

## Training Wheels

Some of the exercises will tell you how many problems there are. Some many even give hints as to what they are or where to look for the solution. The last few exercises in each environment, however, will not have any such hints.

## Why This Was Created

My partner, my soulmate, my moon, my stars, my reason for being the best person I can be is in school to become a web developer. She needs a little help with learning how to troubleshoot but I know she can do it. To that end, I have created these exercises. If others find these exercises helpful, feel free to fork this repository and play along.

## Getting Started

Fork this repository then open [index.html](index.html) in your web browser. I would suggest doing it using Live Server in Visual Studio Code for quick testing.